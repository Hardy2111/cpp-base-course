#include "poly.h"
#include "algorithm"
#include <iterator>
#include <cmath>

void Poly::EraseZeros(Poly& poly) {
    std::erase_if(poly.poly_map_vector_, [](auto& x) { return x.second == 0; });
}

int64_t Poly::operator()(int64_t x) const {
    int64_t answer = 0;
    for (const auto& [power, coeff] : poly_map_vector_) {
        answer += coeff * powl(x, power);
    }
    return answer;
}

Poly Poly::operator+(const Poly& other) {
    Poly answer(this->poly_map_vector_);
    for (const auto& [power, coeff] : poly_map_vector_) {
        answer.poly_map_vector_[power] += coeff;
    }
    EraseZeros(answer);
    return answer;
}

Poly Poly::operator-(const Poly& other) {
    Poly answer(poly_map_vector_);
    for (auto i : other.poly_map_vector_) {
        answer.poly_map_vector_[i.first] -= i.second;
    }
    EraseZeros(answer);
    return answer;
}

Poly Poly::operator-=(const Poly& other) {
    for (const auto& [power, coeff] : other.poly_map_vector_) {
        poly_map_vector_[power] -= coeff;
    }
    EraseZeros(*this);
    return *this;
}

Poly Poly::operator+=(const Poly& other) {
    for (const auto& [power, coeff] : other.poly_map_vector_) {
        poly_map_vector_[power] += coeff;
    }
    EraseZeros(*this);
    return *this;
}

Poly Poly::operator*(const Poly& other) {
    Poly answer;
    for (const auto& [lhspower, lhscoeff] : poly_map_vector_) {
        Poly equal;
        for (const auto& [rhspower, rhscoeff] : other.poly_map_vector_) {
            equal.poly_map_vector_[lhspower + rhspower] = {lhscoeff * rhscoeff};
        }
        answer += equal;
    }
    EraseZeros(answer);
    return answer;
}

Poly Poly::operator-() const {
    Poly answer(poly_map_vector_);
    std::for_each(answer.poly_map_vector_.begin(), answer.poly_map_vector_.end(),
                  [](auto& x) { x.second = -x.second; });
    return answer;
}

std::ostream& operator<<(std::ostream& out, const Poly& poly) {
    if (poly.poly_map_vector_.empty()) {
        out << "y = 0";
        return out;
    }
    out << "y = ";
    for (auto it = poly.poly_map_vector_.rbegin(); it != poly.poly_map_vector_.rend(); ++it) {
        if (it->first == 0) {
            out << std::to_string(it->second);
        } else {
            out << std::to_string(it->second) + "x^" + std::to_string(it->first);
        }
        if (std::prev(it, -1) != poly.poly_map_vector_.rend()) {
            out << " + ";
        }
    }
    return out;
}
