#include "sort_students.h"
#include <tuple>

void SortStudents(std::vector<Student> &students, SortKind sortKind) {
    if (sortKind == SortKind::Date) {
        std::sort(students.begin(), students.end(), [](const auto &x, const auto &y) {
            auto tup_of_date_and_name_1 =
                std::tie(x.birth_date.year, x.birth_date.month, x.birth_date.day, x.last_name, x.name);
            auto tup_of_date_and_name_2 =
                std::tie(y.birth_date.year, y.birth_date.month, y.birth_date.day, y.last_name, y.name);
            return tup_of_date_and_name_1 < tup_of_date_and_name_2;
        });
    } else {
        std::sort(students.begin(), students.end(), [](const auto &x, const auto &y) {
            auto tup_of_name_and_date_1 =
                std::tie(x.last_name, x.name, x.birth_date.year, x.birth_date.month, x.birth_date.day);
            auto tup_of_name_and_date_2 =
                std::tie(y.last_name, y.name, y.birth_date.year, y.birth_date.month, y.birth_date.day);
            return tup_of_name_and_date_1 < tup_of_name_and_date_2;
        });
    }
}