#pragma once

#include <string_view>
#include <vector>

class SearchEngine {
public:
    void BuildIndex(std::string_view text);
    std::vector<std::string_view> Search(std::string_view query, size_t results_count);

private:
    std::vector<std::string_view> SplitAlpha(const std::string_view& dir) const;
    std::vector<std::string_view> Split(const std::string_view& dir, char delimiter) const;
    bool IsLowerEqual(std::string_view lhs, std::string_view rhs) const;
    std::string_view text_;
    std::vector<std::string_view> text_vector_;
};
