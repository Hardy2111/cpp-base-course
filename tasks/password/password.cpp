#include "password.h"
#include "string"

bool ValidatePassword(const std::string &password) {
    int size_of_password = password.size();
    bool flag_for_cap_let = false;
    bool flag_for_lower_let = false;
    bool flag_for_dig = false;
    bool flag_for_another_sym = false;
    if (size_of_password > 14 || size_of_password < 8) {
        return false;
    }
    for (char letter : password) {
        if (letter >= 'A' && letter <= 'Z') {
            flag_for_cap_let = true;
        } else if (letter >= 'a' && letter <= 'z') {
            flag_for_lower_let = true;
        } else if (letter >= '0' && letter <= '9') {
            flag_for_dig = true;
        } else if (letter >= 33 && letter <= 126) {
            flag_for_another_sym = true;
        } else {
            return false;
        }
    }
    if (flag_for_another_sym + flag_for_dig + flag_for_cap_let + flag_for_lower_let < 3) {
        return false;
    }
    return true;
}
