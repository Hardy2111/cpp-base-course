#include "unixpath.h"

std::vector<std::string> UnixPath::Split(std::string dir) const {
    std::vector<std::string> answer;
    std::string word;
    size_t position = 0;

    while ((position = dir.find("/")) != std::string::npos) {
        word = dir.substr(0, position);
        answer.push_back(word);
        dir.erase(0, position + 1);
    }

    answer.push_back(dir);
    return answer;
}

void UnixPath::ChangeDirectory(std::string path) {
    std::string path_dir;
    int dots_value = 0;
    for (size_t i = 1; i < path.size(); ++i) {
        if (path[i] == '.' && path[i - 1] == '.') {
            path_dir.push_back('.');
            path_dir.push_back('.');
        } else if (path[i] != '/' && path[i] != '.') {
            if (path[i - 1] != '/') {
                path_dir.push_back(path[i]);
            } else {
                path_dir.push_back('/');
                path_dir.push_back(path[i]);
            }
        }
    }
    auto iter = path_dir.end();
    while (iter != path_dir.begin() - 1) {
        --iter;
        if (*iter == '.') {
            while (*iter != '/' && iter != path_dir.begin() - 1) {
                --iter;
            }
            if (iter != path_dir.begin() - 1) {
                path_dir.erase(iter, path_dir.end());
            }
        }
    }
    for (size_t i = 1; i < path_dir.size(); ++i) {
        if (path_dir[i] == '.' && path_dir[i - 1] == '.') {
            dots_value += 1;
        }
    }
    if (path_dir.find('/') == std::string::npos) {
        path_dir = "";
    } else {
        path_dir = path_dir.substr(path_dir.find('/'));
    }
    for (auto it = current_dir_.end() - 1; it != current_dir_.begin() - 1; --it) {
        if (dots_value != 0) {
            if (*it == '/') {
                current_dir_.erase(it, current_dir_.end());
                dots_value -= 1;
            }
        } else {
            break;
        }
    }

    auto dir_vector = Split(current_dir_);
    auto answer_vector = Split(path_dir);
    for (size_t i = 0; i < answer_vector.size(); ++i) {
        if (std::find(dir_vector.begin() + 1, dir_vector.end(), answer_vector[i]) != dir_vector.end()) {
            current_dir_.clear();
        }
    }
    if (current_dir_ == "/") {
        current_dir_.clear();
    } else if (current_dir_.empty() && path_dir.empty()) {
        current_dir_ = "/";
    }
    current_dir_ += path_dir;
}

std::string UnixPath::GetAbsolutePath() {
    return current_dir_;
}
std::string UnixPath::GetRelativePath() {
    std::vector<std::string> vec_current_dir = Split(current_dir_);
    std::vector<std::string> home_dir = Split(home_dir_);
    std::string answer;
    bool flag = true;
    if (home_dir_ != current_dir_) {
        if (std::find(vec_current_dir.begin(), vec_current_dir.end(), home_dir.back()) == vec_current_dir.end()) {
            for (auto it = home_dir.end() - 1; it != home_dir.begin() - 1; --it) {
                if (std::find(vec_current_dir.begin(), vec_current_dir.end(), *it) == vec_current_dir.end()) {
                    answer += "../";
                } else {
                    flag = false;
                    for (auto iteration = std::find(vec_current_dir.begin(), vec_current_dir.end(), *it) + 1;
                         iteration != vec_current_dir.end(); ++iteration) {
                        answer += *iteration;
                    }
                }
            }
            if (flag) {
                for (auto i = 0; i < vec_current_dir.size(); ++i) {
                    answer += '/' + vec_current_dir[i];
                }
            }
        } else {
            answer = "./";
            for (auto iteration = std::find(vec_current_dir.begin(), vec_current_dir.end(), home_dir.back()) + 1;
                 iteration != vec_current_dir.end(); ++iteration) {
                answer += *iteration;
            }
        }
    } else {
        answer = '.';
    }
    return answer;
}
