#pragma once
#include <vector>
#include "map"
#include "iostream"

class Poly {
public:
    Poly(const std::vector<int64_t>& new_poly_vector) {
        auto poly_vector = new_poly_vector;
        for (size_t i = 0; i < poly_vector.size(); ++i) {
            if (poly_vector[i]) {
                poly_map_vector_[i] = poly_vector[i];
            }
        }
    };
    Poly(){};
    Poly(const std::map<int64_t, int64_t>& new_poly_vector) : poly_map_vector_(new_poly_vector){};

    int64_t operator()(int64_t x) const;

    bool operator==(const Poly& other) const {
        return other.poly_map_vector_ == poly_map_vector_;
    };

    Poly operator+(const Poly& other);
    Poly operator-() const;
    Poly operator-(const Poly& other);
    Poly operator-=(const Poly& other);
    Poly operator+=(const Poly& other);
    Poly operator*(const Poly& other);
    friend std::ostream& operator<<(std::ostream& out, const Poly& poly);

private:
    void EraseZeros(Poly& poly);
    std::map<int64_t, int64_t> poly_map_vector_;
};
