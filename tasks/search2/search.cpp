#include "search.h"
#include <string>
#include "vector"
#include "map"
#include <math.h>
#include "algorithm"

void SearchEngine::BuildIndex(std::string_view text) {
    text_ = text;
    text_vector_ = Split(text_, '\n');
}

std::vector<std::string_view> SearchEngine::Split(const std::string_view& dir, char delimiter) const {
    std::vector<std::string_view> answer;
    size_t iter_1 = 0;
    size_t iter_2 = 0;
    if (std::find(dir.begin(), dir.end(), delimiter) != dir.end()) {
        while (iter_2 < dir.size()) {
            iter_2 = dir.find(delimiter, iter_1);
            if (std::any_of(dir.substr(iter_1, iter_2 - iter_1).begin(), dir.substr(iter_1, iter_2 - iter_1).end(),
                            [](auto x) { return std::isalpha(x); })) {
                answer.push_back(dir.substr(iter_1, iter_2 - iter_1));
            }
            iter_1 = iter_2 + 1;
        }
        return answer;
    }
    answer.push_back(dir);
    return answer;
}

std::vector<std::string_view> SearchEngine::SplitAlpha(const std::string_view& dir) const {
    std::vector<std::string_view> answer;
    size_t iter_1 = 0;
    size_t iter_2 = 0;
    while (iter_2 < dir.size()) {
        if (std::isalpha(dir[iter_2])) {
            iter_2 += 1;
        } else {
            if (std::isalpha(dir[iter_1]) && std::isalpha(dir[iter_2 - 1])) {
                answer.push_back(dir.substr(iter_1, iter_2 - iter_1));
            }
            iter_2 += 1;
            iter_1 = iter_2;
        }
    }
    if (std::isalpha(dir[iter_1]) && std::isalpha(dir[iter_2 - 1])) {
        answer.push_back(dir.substr(iter_1, iter_2 - iter_1));
    }
    return answer;
}

bool SearchEngine::IsLowerEqual(std::string_view lhs, std::string_view rhs) const {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    for (auto i = 0; i < lhs.size(); ++i) {
        if (tolower(lhs[i]) != tolower(rhs[i])) {
            return false;
        }
    }
    return true;
}

std::vector<std::string_view> SearchEngine::Search(std::string_view query, size_t results_count) {
    std::vector<std::string_view> query_vector = SplitAlpha(query);
    std::vector<std::vector<std::string_view>> vector_of_words = {};
    for (auto line : text_vector_) {
        vector_of_words.push_back(SplitAlpha(line));
    }
    std::map<std::string_view, float> map_of_idf;
    std::map<std::string_view, std::vector<float>> map_of_tf;
    size_t count = 0;
    for (auto it = query_vector.begin(); it < query_vector.end(); ++it) {
        map_of_idf[*it] = 0;
        map_of_tf[*it] = {};
        for (size_t i = 0; i < vector_of_words.size(); ++i) {
            count = 0;
            for (auto j = 0; j < vector_of_words[i].size(); ++j) {
                count += IsLowerEqual(vector_of_words[i][j], *it);
            }
            if (count > 0) {
                map_of_idf[*it] += 1;
                map_of_tf[*it].push_back(static_cast<float>(count) / vector_of_words[i].size());
            } else {
                map_of_tf[*it].push_back(0);
            }
        }
        if (map_of_idf[*it] != 0) {
            map_of_idf[*it] = log(static_cast<float>(vector_of_words.size()) / map_of_idf[*it]);
        } else {
            map_of_idf[*it] = 0;
        }
    }
    std::vector<std::pair<int, float>> ans(text_vector_.size());
    std::pair<int, float> a = {0, 0};
    std::fill(ans.begin(), ans.end(), a);
    for (auto i : map_of_idf) {
        for (size_t j = 0; j < text_vector_.size(); ++j) {
            ans[j] = {j, ans[j].second + map_of_idf[i.first] * map_of_tf[i.first][j]};
        }
    }
    std::sort(ans.begin(), ans.end(), [](auto& x, auto& y) {
        if (x.second == y.second) {
            return x.first < y.first;
        } else {
            return x.second > y.second;
        }
    });
    std::vector<std::string_view> answer;
    for (size_t i = 0; i < std::min(results_count, ans.size()); ++i) {
        if (ans[i].second != 0) {
            answer.push_back(text_vector_[ans[i].first]);
        }
    }
    return answer;
}
