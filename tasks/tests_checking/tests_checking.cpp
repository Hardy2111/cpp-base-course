#include "tests_checking.h"
#include "deque"
#include "vector"

std::vector<std::string> StudentsOrder(const std::vector<StudentAction>& student_actions,
                                       const std::vector<size_t>& queries) {
    std::deque<std::string> leafs;
    for (auto& action : student_actions) {
        if (action.side == Side::Top) {
            leafs.push_back(action.name);
        } else {
            leafs.push_front(action.name);
        }
    }
    std::vector<std::string> answer;
    for (auto& querie : queries) {
        answer.push_back(leafs[leafs.size() - querie]);
    }
    return answer;
}
