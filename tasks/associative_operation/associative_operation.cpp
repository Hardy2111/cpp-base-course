#include "associative_operation.h"

bool IsAssociative(const std::vector<std::vector<size_t>>& table) {
    for (size_t ch_1 = 0; ch_1 < table.size(); ++ch_1) {
        for (size_t ch_2 = 0; ch_2 < table.size(); ++ch_2) {
            for (size_t ch_3 = 0; ch_3 < table.size(); ++ch_3) {
                if (table[ch_1][table[ch_2][ch_3]] != table[table[ch_1][ch_2]][ch_3]) {
                    return false;
                }
            }
        }
    }
    return true;
}
