#pragma once
#include "iterator"

template <typename Iter1, typename Iter2>
class ZipIter {
private:
    Iter1 lhsiter_;
    Iter2 rhsiter_;
    using ZippedPair = std::pair<const decltype(*lhsiter_)&, const decltype(*rhsiter_)&>;

public:
    ZipIter(Iter1 iterator1, Iter2 iterator2) : lhsiter_{iterator1}, rhsiter_{iterator2} {};

    ZipIter& operator++() {
        ++lhsiter_;
        ++rhsiter_;
        return *this;
    }

    bool operator!=(const ZipIter& other) const {
        return rhsiter_ != other.rhsiter_ && lhsiter_ != other.lhsiter_;
    }

    bool operator==(const ZipIter& other) const {
        return rhsiter_ == other.rhsiter_ && lhsiter_ == other.lhsiter_;
    }

    ZippedPair operator*() const {
        return {*lhsiter_, *rhsiter_};
    }
};

template <typename Iter1, typename Iter2>
class Zipped {
private:
    ZipIter<Iter1, Iter2> it_begin_;
    ZipIter<Iter1, Iter2> it_end_;

public:
    Zipped(Iter1 a_begin, Iter2 b_begin, Iter1 a_end, Iter2 b_end)
        : it_begin_{a_begin, b_begin}, it_end_{a_end, b_end} {};

    ZipIter<Iter1, Iter2> begin() {
        return it_begin_;
    }

    ZipIter<Iter1, Iter2> end() {
        return it_end_;
    }
};

template <typename Sequence1, typename Sequence2>
auto Zip(const Sequence1& sequence1, const Sequence2& sequence2) {
    return Zipped(std::begin(sequence1), std::begin(sequence2), std::end(sequence1), std::end(sequence2));
}
