#pragma once
#include "ImageBMP.h"



class ReaderAndWriter {
public:
    ReaderAndWriter(ImageBMP& image, std::ifstream& stream);
    void Write(ImageBMP& image, std::ofstream& stream);
};
