#pragma once

#include <string>
#include <vector>
#include "tuple"

class Minesweeper {
public:
    struct Cell {
        size_t x = 0;
        size_t y = 0;
    };

    struct Status {
        bool has_mine;
        bool has_flag;
        bool is_open;
    };

    enum class GameStatus {
        NOT_STARTED,
        IN_PROGRESS,
        VICTORY,
        DEFEAT,
    };

    using RenderedField = std::vector<std::string>;

    Minesweeper(size_t width, size_t height, size_t mines_count) {
        NewGame(width, height, mines_count);
    };
    Minesweeper(size_t width, size_t height, const std::vector<Cell>& cells_with_mines) {
        NewGame(width, height, cells_with_mines);
    };

    void NewGame(size_t width, size_t height, const std::vector<Cell>& cells_with_mines);
    void NewGame(size_t width, size_t height, size_t mines_count);

    void OpenCell(const Cell& cell);
    void MarkCell(const Cell& cell);

    GameStatus GetGameStatus() const;
    time_t GetGameTime();
    RenderedField RenderField();

private:
    void OpenAllCells(std::vector<std::vector<Status>>& line);
    void InitNewGame(size_t width, size_t height);
    std::vector<Cell> GetAdjacentCells(const Cell& cell) const;
    std::vector<std::vector<Status>> field_;
    size_t width_;
    size_t height_;
    time_t time_ = 0;
    GameStatus game_status_ = GameStatus::NOT_STARTED;
    size_t open_for_win_;
};
