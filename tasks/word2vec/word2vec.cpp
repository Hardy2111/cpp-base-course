#include "word2vec.h"

#include <vector>

int64_t VectorProduct(const std::vector<int>& vectors_1, const std::vector<int>& vectors_2) {
    int64_t answer = 0;
    for (size_t i = 0; i < vectors_1.size(); ++i) {
        answer += static_cast<int64_t>(vectors_1[i]) * vectors_2[i];
    }
    return answer;
}

std::vector<std::string> FindClosestWords(const std::vector<std::string>& words,
                                          const std::vector<std::vector<int>>& vectors) {
    int64_t max_equal = std::numeric_limits<int64_t>::min();
    std::vector<std::string> answer;
    for (size_t i = 1; i < vectors.size(); ++i) {
        if (VectorProduct(vectors[0], vectors[i]) > max_equal) {
            answer.clear();
            answer.push_back(words[i]);
            max_equal = VectorProduct(vectors[0], vectors[i]);
        } else if (VectorProduct(vectors[0], vectors[i]) == max_equal) {
            answer.push_back(words[i]);
        }
    }
    return answer;
}
