add_library(contrib_catch_main
  contrib/catch/catch_main.cpp ../projects/image_processor/ImageBMP.h ../projects/image_processor/Filters.cpp ../projects/image_processor/Filters.h ../projects/image_processor/ReaderAndWriter.cpp ../projects/image_processor/ReaderAndWriter.h ../projects/image_processor/Parser.cpp ../projects/image_processor/Parser.h ../projects/image_processor/test.cpp)

target_include_directories(contrib_catch_main
  PUBLIC contrib/catch)
