#include "scorer.h"

void Scorer::OnMergeRequestClosed(const StudentName &student_name, const TaskName &task_name) {
    students_and_tasks[student_name][task_name].second = false;
}

void Scorer::OnMergeRequestOpen(const StudentName &student_name, const TaskName &task_name) {
    students_and_tasks[student_name][task_name].second = true;
}

void Scorer::OnCheckSuccess(const StudentName &student_name, const TaskName &task_name) {
    students_and_tasks[student_name][task_name].first = true;
}

void Scorer::Reset() {
    students.clear();
    students_and_tasks.clear();
}

void Scorer::OnCheckFailed(const StudentName &student_name, const TaskName &task_name) {
    students_and_tasks[student_name][task_name].first = false;
}

ScoreTable Scorer::GetScoreTable() {
    for (auto &student : students_and_tasks) {
        for (auto &task : student.second) {
            if (task.second.first && !task.second.second) {
                students[student.first].insert(task.first);
            }
        }
    }
    return students;
}