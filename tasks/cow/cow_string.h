#pragma once
#include "string"
#include "iostream"

class Iter;
class ConstIter;

class Str {
public:
    std::string data;
    size_t cnt;

    Str(std::string text) {
        cnt = 1;
        data = text;
    }

};

class CowString {
public:
    Str* ptr;

    CowString(std::string text);
    CowString(const CowString& other);
    CowString(CowString&& other);
    CowString& operator=(CowString& other);
    CowString& operator=(CowString&& other);
    CowString& operator=(const CowString& other);
    const char* GetData() const;
    const char* GetData();
    ~CowString();
    CowString& operator+=(CowString& other);
    CowString& operator+=(std::string view_text);
    char& operator[](size_t index);
    const char& At(size_t index) const;
    ConstIter& begin() const;
    ConstIter& end() const;
    Iter& begin();
    Iter& End();
    bool operator==(std::string text) const;
    bool operator==(CowString& other) const;
};

class Iter {
public:
    CowString* cowptr;
    size_t index;

    Iter(CowString* pstr, size_t ind);

    operator char() {
        return cowptr->ptr->data[index];
    }

    operator char() const {
        return cowptr->ptr->data[index];
    }

    Iter& operator*() {
        return *this;
    }

    Iter& operator=(char litter);

};

class ConstIter {
public:
    const CowString* ccowptr;
    size_t index;

    ConstIter(const CowString* pstr, size_t ind);

    operator char() const {
        return ccowptr->ptr->data[index];
    }

    ConstIter& operator*() {
        return *this;
    }


    ConstIter& operator++() {
        ++index;
        return *this;
    }
};