#include "ReaderAndWriter.h"
#include "Parser.h"



int main(int argc, char* argv[]) {
    Parser parser(argc, argv);
    ImageBMP image_bmp;
    ReaderAndWriter readerandwriter(image_bmp, parser.istream);
    image_bmp = parser.DoFilters(image_bmp);
    readerandwriter.Write(image_bmp, parser.ostream);
    return 0;
}
