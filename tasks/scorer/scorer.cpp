#include "scorer.h"

ScoreTable GetScoredStudents(const Events& events, time_t score_time) {
    ScoreTable answer;
    std::vector<const Event*> sorted_events(events.size());
    std::map<StudentName, std::map<TaskName, std::vector<bool>>> students_and_tasks;

    for (size_t i = 0; i < events.size(); ++i) {
        students_and_tasks[events[i].student_name][events[i].task_name] = {false, true};
        sorted_events[i] = &events[i];
    }
    std::sort(sorted_events.begin(), sorted_events.end(),
              [](const Event* x, const Event* y) { return x->time < y->time; });

    for (auto& event : sorted_events) {
        if (event->time <= score_time) {
            if (event->event_type == EventType::MergeRequestOpen) {
                students_and_tasks[event->student_name][event->task_name][1] = false;
            } else if (event->event_type == EventType::MergeRequestClosed) {
                students_and_tasks[event->student_name][event->task_name][1] = true;
            } else if (event->event_type == EventType::CheckSuccess) {
                students_and_tasks[event->student_name][event->task_name][0] = true;
            } else if (event->event_type == EventType::CheckFailed) {
                students_and_tasks[event->student_name][event->task_name][0] = false;
            }
        }
    }
    for (auto& student : students_and_tasks) {
        for (auto& task : students_and_tasks[student.first]) {
            if (students_and_tasks[student.first][task.first][0] && students_and_tasks[student.first][task.first][1]) {
                answer[student.first].insert(task.first);
            }
        }
    }
    return answer;
}
