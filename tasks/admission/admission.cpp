#include "admission.h"

AdmissionTable FillUniversities(const std::vector<University>& universities, const std::vector<Applicant>& applicants) {

    std::vector<const Applicant*> sort_applicants(applicants.size());
    for (size_t i = 0; i < applicants.size(); ++i) {
        sort_applicants[i] = &applicants[i];
    }

    std::sort(sort_applicants.begin(), sort_applicants.end(), [](const Applicant* x, const Applicant* y) {
        if (x->points == y->points) {
            return std::tie(x->student.birth_date.year, x->student.birth_date.month, x->student.birth_date.day,
                            x->student.name) > std::tie(y->student.birth_date.year, y->student.birth_date.month,
                                                        y->student.birth_date.day, y->student.name);
        }
        return x->points > y->points;
    });

    AdmissionTable answer;

    for (auto univ : universities) {
        answer[univ.name] = {};
    }

    bool flag;
    for (auto& enrollee : sort_applicants) {
        flag = false;
        for (auto& institute : enrollee->wish_list) {
            if (flag) {
                break;
            }
            for (auto& struct_university : universities) {
                if (struct_university.name == institute &&
                    answer[struct_university.name].size() < struct_university.max_students) {
                    answer[struct_university.name].push_back(&enrollee->student);
                    flag = true;
                }
            }
        }
    }

    for (auto& i : answer) {
        std::sort(answer[i.first].begin(), answer[i.first].end(), [](const Student* x, const Student* y) {
            return std::tie(x->name, x->birth_date.year, x->birth_date.month, x->birth_date.day) <
                   std::tie(y->name, y->birth_date.year, y->birth_date.month, y->birth_date.day);
        });
    }

    return answer;
}
