#include "palindrome.h"

bool IsPalindrome(const std::string &str) {
    int ch_1 = 0;
    int ch_2 = str.size() - 1;
    while (ch_1 < ch_2) {
        if (str[ch_1] == ' ') {
            ++ch_1;
        } else if (str[ch_2] == ' ') {
            --ch_2;
        } else if (str[ch_1] == str[ch_2]) {
            ++ch_1;
            --ch_2;
        } else {
            return false;
        }
    }
    return true;
}