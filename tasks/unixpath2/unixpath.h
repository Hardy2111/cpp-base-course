#pragma once
#include <vector>
#include <string>

class UnixPath {
public:
    UnixPath(std::string initial_dir) {
        home_dir_ = initial_dir;
        current_dir_ = initial_dir;
    };
    void ChangeDirectory(std::string path);
    std::string GetAbsolutePath();
    std::string GetRelativePath();

private:
    std::vector<std::string> Split(std::string dir) const;
    std::string home_dir_;
    std::string current_dir_;
};
