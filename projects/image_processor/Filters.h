#pragma once
#include "ImageBMP.h"
#include "cmath"

class Filters {
public:
    virtual ImageBMP Apply(const ImageBMP& image) = 0;
    virtual ~Filters() = default;
};

class Crop : public Filters {
private:
    size_t width_;
    size_t height_;
public:
    Crop(size_t width, size_t height) : width_(width), height_(height) {};
    ImageBMP Apply(const ImageBMP& image) override;
};

class GrayScale : public Filters {
public:
    ImageBMP Apply(const ImageBMP& image) override;
};

class Negative : public Filters {
public:
    ImageBMP Apply(const ImageBMP& image) override;
};

class Sharpening : public Filters {
public:
    ImageBMP Apply(const ImageBMP& image) override;
};

class EdgeDetection : public Filters {
private:
    double threshold_;
public:
    ImageBMP Apply(const ImageBMP& image) override;
    EdgeDetection(double threshold) : threshold_(threshold) {};
};

class Copy : public Filters {
public:
    ImageBMP Apply(const ImageBMP& image) override;
};

class Gaussian : public Filters {
private:
    void CalcGauss(double sigma, uint32_t height_width, uint32_t width_height,
                   const std::vector<std::vector<Pixel>>& const_pixels, std::vector<std::vector<Pixel>>& pixels, bool apply_by_rows);
    double Denominator(double sigma);
    size_t sigma_;
public:
    Gaussian(double sigma) : sigma_(sigma) {};
    ImageBMP Apply(const ImageBMP& image) override;
};

class CustomeFilter : public Filters {
private:
    size_t equal_;
public:
    ImageBMP Apply(const ImageBMP& image) override;
    CustomeFilter(size_t equal) : equal_(equal) {};
};
