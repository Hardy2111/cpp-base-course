#pragma once

#include <forward_list>
#include <string>

using Value = std::string;
using Iterator = std::forward_list<std::string>::const_iterator;
using ZippedPair = std::pair<const Value&, const Value&>;

class ZipIter {
private:
    Iterator rhsiter_;
    Iterator lhsiter_;
public:
    ZipIter(Iterator iterator1, Iterator iterator2) : rhsiter_{iterator1}, lhsiter_{iterator2} {};

    ZipIter& operator++() {
        rhsiter_++;
        lhsiter_++;
        return *this;
    }

    bool operator!=(const ZipIter& other) const {
        return rhsiter_ != other.rhsiter_ && lhsiter_ != other.lhsiter_;
    }

    bool operator==(const ZipIter& other) const {
        return rhsiter_ == other.rhsiter_ && lhsiter_ == other.lhsiter_;
    }

    ZippedPair operator*() const {
        return {*rhsiter_, *lhsiter_};
    }
};
class Zipped {
private:
    ZipIter it_begin_;
    ZipIter it_end_;

public:
    Zipped(Iterator a_begin, Iterator a_end, Iterator b_begin, Iterator b_end)
        : it_begin_(a_begin, b_begin), it_end_(a_end, b_end){};

    ZipIter begin() {
        return it_begin_;
    }

    ZipIter end() {
        return it_end_;
    }
};

Zipped Zip(Iterator a_begin, Iterator a_end, Iterator b_begin, Iterator b_end);
