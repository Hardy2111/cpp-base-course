#include "cow_string.h"

CowString& CowString::operator=(CowString&& other) {
    ptr = nullptr;
    std::swap(other.ptr, ptr);
    return *this;
}

CowString& CowString::operator=(CowString& other) {
    ptr = nullptr;
    ptr = other.ptr;
    ++ptr->cnt;
    return *this;
}

CowString::CowString(CowString&& other) {
    ptr = nullptr;
    std::swap(other.ptr, ptr);
}

CowString::CowString(const CowString& other) {
    ptr = other.ptr;
    ++ptr->cnt;
}

CowString::CowString(std::string text) {
    ptr = new Str(text);
}

CowString& CowString::operator+=(CowString& other) {
    --ptr->cnt;
    auto tmpstr = ptr->data + other.ptr->data;
    if (ptr->cnt == 0) {
        delete ptr;
    }
    ptr = new Str(tmpstr);
    return *this;
}

CowString& CowString::operator+=(std::string view_text) {
    --ptr->cnt;
    auto tmpstr = ptr->data + view_text;
    if (ptr->cnt == 0) {
        delete ptr;
    }
    ptr = new Str(tmpstr);
    return *this;
}


bool CowString::operator==(std::string text) const {
    return text == ptr->data;
}

CowString& CowString::operator=(const CowString& other) {
    delete ptr;
    ptr = other.ptr;
    ++ptr->cnt;
    return *this;
}

ConstIter& CowString::begin() const {
    return *ConstIter(this, 0);
}

ConstIter& CowString::end() const {
    return *ConstIter(this, ptr->data.size());
}

bool CowString::operator==(CowString& other) const {
    return ptr->data == other.ptr->data;
}

Iter& CowString::begin() {
    return *Iter(this, 0);
}

Iter& CowString::End() {
    return *Iter(this, ptr->data.size());
}

char& CowString::operator[](size_t index) {
    return ptr->data[index];
}
const char* CowString::GetData() const  {
    return ptr->data.data();
}
const char* CowString::GetData() {
    return ptr->data.data();
}
CowString::~CowString() {
    if (ptr->cnt <= 1) {
        delete ptr;
    } else {
        --ptr->cnt;
    }
}
const char& CowString::At(size_t index) const {
    return ptr->data[index];
}

Iter::Iter(CowString* pstr, size_t ind) {
    cowptr = pstr;
    index = ind;
}

Iter& Iter::operator=(char litter) {
    --cowptr->ptr->cnt;
    auto tmpdata = cowptr->ptr->data;
    if (cowptr->ptr->cnt <= 0) {
        delete cowptr->ptr;
    }
    cowptr->ptr = new Str(tmpdata);
    cowptr->ptr->data[index] = litter;
    return *this;
}

ConstIter::ConstIter(const CowString* pstr, size_t ind) {
    ccowptr = pstr;
    index = ind;
}
