#include "minesweeper.h"
#include "algorithm"
#include "random"
#include <ctime>

void Minesweeper::InitNewGame(size_t width, size_t height) {
    game_status_ = GameStatus::IN_PROGRESS;
    width_ = width;
    height_ = height;
    field_.resize((height_ + 2));
    std::vector<Status> lines(width_ + 2);
    Status for_fill = {.has_mine = false, .has_flag = false, .is_open = false};
    std::fill(lines.begin(), lines.end(), for_fill);
    std::fill(field_.begin(), field_.end(), lines);
    std::vector<Status> first_last_line(width_ + 2);
    for_fill.is_open = true;
    std::fill(first_last_line.begin(), first_last_line.end(), for_fill);
    for (auto &line : field_) {
        line[0].is_open = true;
        line.back().is_open = true;
    }
    field_[0] = first_last_line;
    field_.back() = first_last_line;
}
void Minesweeper::NewGame(size_t width, size_t height, const std::vector<Cell> &cells_with_mines) {
    InitNewGame(width, height);
    open_for_win_ = width_ * height_ - cells_with_mines.size();
    for (const auto &mines : cells_with_mines) {
        field_[mines.y + 1][mines.x + 1].has_mine = true;
    }
}
void Minesweeper::NewGame(size_t width, size_t height, size_t mines_count) {
    InitNewGame(width, height);
    open_for_win_ = width_ * height_ - mines_count;
    std::vector<int> random_mines(height_ * width_);
    for (size_t i = 0; i < random_mines.size(); ++i) {
        random_mines[i] = i;
    }
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(random_mines.begin(), random_mines.end(), g);
    for (size_t i = 0; i < mines_count; ++i) {
        field_[random_mines[i] / width + 1][random_mines[i] % width + 1].has_mine = true;
    }
}
void Minesweeper::MarkCell(const Cell &cell) {
    if (game_status_ == GameStatus::VICTORY || game_status_ == GameStatus::DEFEAT) {
        return;
    }
    if (time_ == 0) {
        time_ = std::time(nullptr);
    }
    field_[cell.y + 1][cell.x + 1].has_flag = !field_[cell.y + 1][cell.x + 1].has_flag;
}

void Minesweeper::OpenAllCells(std::vector<std::vector<Status>> &line) {
    std::for_each(field_.begin(), field_.end(), [](auto &cell) {
        std::for_each(cell.begin(), cell.end(), [](auto &y) { y.is_open = true; });
        return cell;
    });
}
time_t Minesweeper::GetGameTime() {
    if (time_ == 0) {
        return 0;
    }
    return time_;
}
std::vector<Minesweeper::Cell> Minesweeper::GetAdjacentCells(const Minesweeper::Cell &cell) const {
    std::vector<Minesweeper::Cell> adjacent_cells;
    for (auto y : {std::min(cell.y, height_), std::min(cell.y + 1, height_), std::min(cell.y + 2, height_)}) {
        for (auto x : {std::min(cell.x, width_), std::min(cell.x + 1, width_), std::min(cell.x + 2, width_)}) {
            adjacent_cells.push_back({.x = x, .y = y});
        }
    }
    return adjacent_cells;
}

void Minesweeper::OpenCell(const Cell &cell) {
    if (game_status_ == GameStatus::VICTORY || game_status_ == GameStatus::DEFEAT) {
        return;
    }
    if (time_ == 0) {
        time_ = std::time(nullptr);
    }
    field_[cell.y + 1][cell.x + 1].is_open = true;
    if (field_[cell.y + 1][cell.x + 1].has_flag) {
        return;
    }
    if (field_[cell.y + 1][cell.x + 1].has_mine) {
        OpenAllCells(field_);
        game_status_ = GameStatus::DEFEAT;
        time_ = std::time(nullptr) - time_;
        return;
    }
    field_[cell.y + 1][cell.x + 1].is_open = true;
    open_for_win_ -= 1;
    if (open_for_win_ == 0) {
        game_status_ = GameStatus::VICTORY;
        time_ = std::time(nullptr) - time_;
    }
    for (auto cage : GetAdjacentCells(cell)) {
        if (field_[cage.y][cage.x].has_mine) {
            return;
        }
    }
    Cell box;
    for (auto cage : GetAdjacentCells(cell)) {
        if (!field_[cage.y][cage.x].has_flag && !field_[cage.y][cage.x].is_open) {
            box.y = cage.y - 1;
            box.x = cage.x - 1;
            OpenCell(box);
        }
    }
}

Minesweeper::GameStatus Minesweeper::GetGameStatus() const {
    return game_status_;
}

Minesweeper::RenderedField Minesweeper::RenderField() {
    char count = '0';
    RenderedField rfield(height_);
    std::string for_fill(width_, '-');
    std::fill(rfield.begin(), rfield.end(), for_fill);
    for (size_t i = 1; i < field_.size() - 1; ++i) {
        for (size_t j = 1; j < field_[i].size() - 1; ++j) {
            if (field_[i][j].has_flag) {
                rfield[i - 1][j - 1] = '?';
            } else if (!field_[i][j].is_open) {
                rfield[i - 1][j - 1] = '-';
            } else if (field_[i][j].has_mine) {
                rfield[i - 1][j - 1] = '*';
            } else {
                count = '0';
                for (auto y : {i - 1, i, i + 1}) {
                    for (auto x : {j - 1, j, j + 1}) {
                        if (field_[std::min(height_ + 1, y)][std::min(width_ + 1, x)].has_mine && (y != i || x != j)) {
                            count += 1;
                        }
                    }
                }
                if (count != '0') {
                    rfield[i - 1][j - 1] = count;
                } else {
                    rfield[i - 1][j - 1] = '.';
                }
            }
        }
    }
    return rfield;
}