#pragma once
#include "Filters.h"
#include "map"

class FilterException : public std::exception {
private:
    const char* except_;

public:
    explicit FilterException(const char* data) : except_(data){};
    const char* what() const throw() {
        return except_;
    }
};

class Parser {
public:
    std::vector<Filters*> filters_;
    std::ifstream istream;
    std::ofstream ostream;
    int arc = 0;
    char** arg;
    ~Parser();
    Parser(int argc, char* argv[]);
    ImageBMP DoFilters(ImageBMP& image);
};
