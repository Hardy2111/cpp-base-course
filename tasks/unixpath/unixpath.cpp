#include "unixpath.h"
#include "vector"

std::vector<std::string> Split(std::string dir) {
    std::vector<std::string> answer;
    std::string word;
    size_t position = 0;

    while ((position = dir.find("/")) != std::string::npos) {
        word = dir.substr(0, position);
        answer.push_back(word);
        dir.erase(0, position + 1);
    }

    answer.push_back(dir);
    return answer;
}

std::string NormalizePath(std::string_view current_working_dir, std::string_view path) {
    std::string dir = std::string(current_working_dir);
    std::string path_dir;
    int dots_value = 0;
    for (size_t i = 1; i < path.size(); ++i) {
        if (path[i] == '.' && path[i - 1] == '.') {
            dots_value += 1;
        } else if (path[i] != '/' && path[i] != '.') {
            if (path[i - 1] != '/') {
                path_dir.push_back(path[i]);
            } else {
                path_dir.push_back('/');
                path_dir.push_back(path[i]);
            }
        }
    }
    for (auto it = dir.end() - 1; it != dir.begin() - 1; --it) {
        if (dots_value != 0) {
            if (*it == '/') {
                dir.erase(it, dir.end());
                dots_value -= 1;
            }
        } else {
            break;
        }
    }
    auto dir_vector = Split(dir);
    auto answer_vector = Split(path_dir);
    for (size_t i = 0; i < answer_vector.size(); ++i) {
        if (std::find(dir_vector.begin() + 1, dir_vector.end(), answer_vector[i]) != dir_vector.end()) {
            dir.clear();
        }
    }
    if (dir == "/") {
        dir.clear();
    } else if (dir.empty() && path_dir.empty()) {
        dir = "/";
    }

    return dir + path_dir;
}
