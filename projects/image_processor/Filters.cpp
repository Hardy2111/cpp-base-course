#include "Filters.h"

double Gaussian::Denominator(double sigma) {
    double answer = std::sqrt(2 * M_PI * std::powl(sigma, 2));
    return answer;
}

void Gaussian::CalcGauss(double sigma, uint32_t height_width, uint32_t width_height,
               const std::vector<std::vector<Pixel>>& const_pixels, std::vector<std::vector<Pixel>>& pixels, bool apply_by_rows) {
    double const_coef = (1. / Denominator(sigma));
    double koef;
    for (size_t i = 0; i < height_width; ++i) {
        for (size_t j = 3 * sigma; j < width_height - 3 * sigma; ++j) {
            Pixel temp_pixel{0, 0, 0};
            double summ = 0;
            for (size_t left = j - 3 * sigma; left < j + 3 * sigma; ++left) {
                koef = const_coef * exp(-std::powl(j - left, 2) / (2 * std::powl(sigma, 2)));
                summ += koef;
                if (apply_by_rows == 1) {
                    temp_pixel += const_pixels[i][left] * koef;
                } else {
                    temp_pixel += const_pixels[left][i] * koef;
                }
            }
            if (apply_by_rows) {
                pixels[i][j] = temp_pixel * (1. / summ);
            } else {
                pixels[j][i] = temp_pixel * (1. / summ);
            }
        }
    }
}

void SharpAndDet(std::vector<std::vector<Pixel>>& pixels, const std::vector<std::vector<Pixel>>& const_pixels,
                 size_t equal, size_t i, size_t j) {
    pixels[i][j] = const_pixels[i][j] * equal -
                   (const_pixels[i - 1][j] + const_pixels[i + 1][j] + const_pixels[i][j + 1] + const_pixels[i][j - 1]);
}

void GetNewHeader(uint32_t width, uint32_t height, uint32_t sizebmp, char header[54]) {
    for (auto i = 0; i < 54; ++i) {
        if (i > 1 && i < 6) {
            header[i] = reinterpret_cast<char*>(&sizebmp)[i - 2];
        } else if (i > 17 && i < 22) {
            header[i] = reinterpret_cast<char*>(&width)[i - 18];
        } else if (i > 21 && i < 26) {
            header[i] = reinterpret_cast<char*>(&height)[i - 22];
        }
    }
}


ImageBMP Crop::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    width_ = fmin(image.width_, width_);
    height_ = fmin(image.height_, height_);
    new_image.sizebmp_ = 3 * width_ * height_ + height_ * (4 - (image.width_ * 3) % 4) % 4;
    GetNewHeader(width_, height_, new_image.sizebmp_, new_image.header_);
    std::vector<std::vector<Pixel>> new_pixels;
    for (size_t i = image.height_ - height_; i < image.height_; ++i) {
        std::vector<Pixel> line;
        for (size_t j = 0; j < width_; ++j) {
            line.push_back(new_image.pixels_[i][j]);
        }
        new_pixels.push_back(line);
    }
    new_image.pixels_ = new_pixels;
    new_image.width_ = width_;
    new_image.height_ = height_;
    return new_image;
}

ImageBMP GrayScale::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    for (size_t i = 0; i < new_image.height_; ++i) {
        for (size_t j = 0; j < new_image.width_; ++j) {
            new_image.pixels_[i][j].Red = new_image.pixels_[i][j].Green = new_image.pixels_[i][j].Blue =
                new_image.pixels_[i][j].Red * 0.229 + new_image.pixels_[i][j].Green * 0.587 +
                new_image.pixels_[i][j].Blue * 0.114;
        }
    }
    return new_image;
}

ImageBMP Negative::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    for (size_t i = 0; i < new_image.height_; ++i) {
        for (size_t j = 0; j < new_image.width_; ++j) {
            new_image.pixels_[i][j].Reverse();
        }
    }
    return new_image;
}

ImageBMP Sharpening::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    for (size_t i = 1; i < new_image.height_ - 1; ++i) {
        for (size_t j = 1; j < new_image.width_ - 1; ++j) {
            SharpAndDet(new_image.pixels_, image.pixels_, 5, i, j);
        }
    }
    return new_image;
}

ImageBMP EdgeDetection::Apply(const ImageBMP& image) {
    ImageBMP new_image = GrayScale().Apply(image);
    for (size_t i = 1; i < new_image.height_ - 1; ++i) {
        for (size_t j = 1; j < new_image.width_ - 1; ++j) {
            SharpAndDet(new_image.pixels_, image.pixels_, 4, i, j);
        }
    }
    for (size_t i = 1; i < new_image.height_ - 1; ++i) {
        for (size_t j = 1; j < new_image.width_ - 1; ++j) {
            if (new_image.pixels_[i][j].Red > threshold_) {
                new_image.pixels_[i][j] = 255;
            } else {
                new_image.pixels_[i][j] = 0;
            }
        }
    }
    return new_image;
}


ImageBMP Copy::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    return new_image;
}

ImageBMP Gaussian::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    std::vector<std::vector<Pixel>> temp_pixels = image.pixels_;
    CalcGauss(sigma_, new_image.height_, new_image.width_, image.pixels_, temp_pixels, true);
    CalcGauss(sigma_, new_image.width_, new_image.height_, temp_pixels, new_image.pixels_, false);
    return new_image;
}


ImageBMP CustomeFilter::Apply(const ImageBMP& image) {
    ImageBMP new_image = image;
    for (size_t i = 1; i < new_image.height_ - 1; ++i) {
        for (size_t j = 1; j < new_image.width_ - 1; ++j) {
            int ipixel = i - i % equal_;
            int jpixel = j - j % equal_;
            new_image.pixels_[i][j] = new_image.pixels_[ipixel][jpixel];
        }
    }
    return new_image;
}

