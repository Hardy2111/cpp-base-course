#include "multiplication.h"

#include <stdexcept>

int64_t Multiply(int a, int b) {
    int64_t answer;
    answer = static_cast<int64_t>(a) * b;
    return answer;
}
