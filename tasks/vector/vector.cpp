#include "vector.h"
#include "compare"

Vector::Vector(size_t size) {
    vector_ = new int[size];
    for (size_t i = 0; i < size; ++i) {
        vector_[i] = 0;
    }
    size_ = size;
    capacity_ = size;
}

Vector::Vector(std::initializer_list<ValueType> list) {
    vector_ = new int[list.size()];
    size_t i = 0;
    for (auto it = list.begin(); it != list.end(); ++it, ++i) {
        vector_[i] = *it;
    }
    size_ = list.size();
    capacity_ = list.size();
}

Vector::Vector(const Vector& other) {
    vector_ = new int[other.size_];
    size_ = other.size_;
    capacity_ = other.capacity_;
    for (size_t i = 0; i < size_; ++i) {
        vector_[i] = other.vector_[i];
    }
}

Vector& Vector::operator=(const Vector& other) {
    delete[] this->vector_;
    vector_ = new int[other.size_];
    for (size_t i = 0; i < other.size_; ++i) {
        vector_[i] = other[i];
    }
    size_ = other.size_;
    capacity_ = 2 * other.size_;
    return *this;
}

Vector::~Vector() {
    delete[] this->vector_;
}

Vector::SizeType Vector::Size() const {
    return size_;
}

Vector::SizeType Vector::Capacity() const {
    return capacity_;
}

const Vector::ValueType* Vector::Data() const {
    return vector_;
}

Vector::ValueType& Vector::operator[](size_t position) {
    return vector_[position];
}

Vector::ValueType Vector::operator[](size_t position) const {
    return vector_[position];
}

bool Vector::operator==(const Vector& other) const {
    if (size_ != other.size_) {
        return false;
    }
    for (size_t i = 0; i < size_; ++i) {
        if (vector_[i] != other[i]) {
            return false;
        }
    }
    return true;
}

bool Vector::operator!=(const Vector& other) const {
    if (size_ != other.size_) {
        return true;
    }
    for (size_t i = 0; i < size_; ++i) {
        if (vector_[i] != other[i]) {
            return true;
        }
    }
    return false;
}

void Vector::Reserve(Vector::SizeType new_capacity) {
    if (new_capacity > capacity_) {
        delete[] vector_;
        capacity_ = new_capacity;
        vector_ = new int[capacity_];
    }
}

void Vector::Clear() {
    for (size_t i = 0; i < size_; ++i) {
        vector_[i] = 0;
    }
    size_ = 0;
}

void Vector::PushBack(const Vector::ValueType& new_element) {
    if (size_ == 0 && capacity_ == 0) {
        size_ += 1;
        capacity_ += 1;
        vector_ = new int[size_];
        vector_[0] = new_element;
    } else if (size_ == capacity_) {
        capacity_ = 2 * size_;
        auto relocation_vector = new int[capacity_];
        for (size_t i = 0; i < size_; ++i) {
            relocation_vector[i] = vector_[i];
        }
        relocation_vector[size_] = new_element;
        size_ += 1;
        delete[] vector_;
        vector_ = relocation_vector;
    } else {
        vector_[size_] = new_element;
        size_ += 1;
    }
}

void Vector::PopBack() {
    if (size_ != 0) {
        vector_[size_ - 1] = 0;
        size_ -= 1;
    }
}

void Vector::Swap(Vector& other) {
    std::swap(size_, other.size_);
    std::swap(capacity_, other.capacity_);
    std::swap(vector_, other.vector_);
}

std::strong_ordering Vector::operator<=>(const Vector& other) const {
    auto tmp = std::min(size_, other.size_);
    for (size_t i = 0; i < tmp; ++i) {
        if (auto cmp = vector_[i] <=> other.vector_[i]; cmp != 0) {
            return cmp;
        }
    }
    return size_ <=> other.size_;
}

Vector::Iterator::Iterator(Vector::ValueType* pointer) {
    iter_ = pointer;
}

Vector::Iterator& Vector::Iterator::operator=(Vector::Iterator other) {
    iter_ = other.iter_;
    return *this;
}

Vector::Iterator& Vector::Iterator::operator++() {
    ++iter_;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int) {
    auto tmp = *this;
    ++iter_;
    return tmp;
}

Vector::Iterator& Vector::Iterator::operator--() {
    --iter_;
    return *this;
}

Vector::Iterator Vector::Iterator::operator--(int) {
    auto tmp = *this;
    --iter_;
    return tmp;
}

Vector::Iterator Vector::Iterator::operator+(Vector::DifferenceType shift) {
    auto tmp = *this;
    tmp += shift;
    return tmp;
}

Vector::Iterator& Vector::Iterator::operator+=(Vector::DifferenceType shift) {
    iter_ += shift;
    return *this;
}

Vector::Iterator& Vector::Iterator::operator-=(Vector::DifferenceType shift) {
    iter_ -= shift;
    return *this;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const {
    return iter_ == other.iter_;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const {
    return iter_ != other.iter_;
}

Vector::DifferenceType Vector::Iterator::operator-(Vector::Iterator other) {
    DifferenceType type = iter_ - other.iter_;
    return type;
}

Vector::ValueType& Vector::Iterator::operator*() const {
    return *iter_;
}

Vector::ValueType* Vector::Iterator::operator->() const {
    return iter_;
}
